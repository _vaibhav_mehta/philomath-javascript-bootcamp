# PhiloMath JavaScript BootCamp curriculum

## 1. Master Git and Version control.

#### Course: 
https://www.udacity.com/course/version-control-with-git--ud123

#### Video Tutorial:
https://www.youtube.com/watch?v=8JJ101D3knE

## 2. HTML and CSS Basics Tutorial.

#### How HTML & CSS work together
https://blog.codeanalogies.com/2018/05/09/the-relationship-between-html-css-and-javascript-explained/

#### Course:
https://learn.shayhowe.com/html-css/
##### - Mandatory sections to be covered:
Lesson 1: Building Your First Web Page
Lesson 2: Getting to Know HTML
Lesson 3: Getting to Know CSS
Lesson 4: Opening the Box Model
Lesson 5: Positioning Content - Skip positioning with floats

https://learn.shayhowe.com/advanced-html-css/
##### -Madatory sections to be covered:
Lesson 4: Responsive Web Design
##### -Stretch goals:
Lesson 7: Transforms
Lesson 8: Transitions & Animations

#### Video Tutorial
https://www.youtube.com/watch?v=vQWlgd7hV4A

## 3. CSS Precedence
http://tutorials.jenkov.com/css/precedence.html

## 4. Flexbox with drills and videos 
#### -Cheat sheet
https://css-tricks.com/snippets/css/a-guide-to-flexbox/

#### -Play Game and learn
https://flexboxfroggy.com/

#### -Videos
##### CSS Flexbox in 100 Seconds
https://www.youtube.com/watch?v=K74l26pE4YA

##### Flexbox CSS in 20 minutes
https://www.youtube.com/watch?v=JJSoEo8JSnc

##### Course
https://flexbox.io/

## 5. Grid with drills and videos
#### -Cheat sheet
https://grid.malven.co/
https://css-tricks.com/snippets/css/complete-guide-grid/

#### -Play game and learn
https://cssgridgarden.com/

#### -Course
https://cssgrid.io/

## 6 JavaScript
#### Course:
https://www.codecademy.com/learn/introduction-to-javascript

#### DOM Manupulation
recorded session: <br>
part1- https://drive.google.com/file/d/1o9B7bCCjpnRlAHb2Kz9OAFz9XDxWKWQf/view?usp=sharing <br>
part2- https://drive.google.com/file/d/1pQthuxxZvXY9FyLZYYmIf4FxvkfTF8FC/view?usp=sharing <br>
part3- https://drive.google.com/file/d/1vy9zzPlF2vVgrKywioFJdmj1JfZlNXfH/view?usp=sharing <br>

Youtube video: https://www.youtube.com/watch?v=wiozYyXQEVk 

#### map, reduce, filter
video: https://youtu.be/EzB6Pk66XW8 <br>

#### callbacks, async, await and promises
video: https://www.youtube.com/watch?v=_8gHHBlbziw <br>
Code: https://gitlab.com/_vaibhav_mehta/callbacks_asyncawait_promises_philomath <br>

# Tasks

### Task-1 on Flex
#### url
https://gitlab.com/_vaibhav_mehta/4-cards-task-1
#### Solution: 
https://drive.google.com/file/d/1ZnFWkHOpFGLLsqwP7ZJ33ZQkgTyXbIbf/view?usp=sharing

### Task-2 on Grid
#### url
https://gitlab.com/_vaibhav_mehta/task-2
#### Solution
https://drive.google.com/file/d/125u7qjTed1CQIltu9Rn1c5rQKzKp0Y95/view?usp=sharing

### Task-3 Create a Startup Landing Page should be 100% mobile friendly
#### Solution Task3 (Part1)
https://drive.google.com/file/d/1oJsG6_8AYJKLvlTLDJdyuULf_N8_09FF/view?usp=sharing
#### Solution Task3 (Part2)
https://drive.google.com/file/d/11s5dVxIQpsXQoSCSmvEdcFmEaUqixP7P/view?usp=sharing

### Task-4 Facebook
#### Resources
https://gitlab.com/_vaibhav_mehta/facebook-philomath.git <br>
Video: https://www.loom.com/share/4fa1c5b119fc488ca85710c2ed0167bd


### Task-5 Calculator
#### Resource
https://gitlab.com/_vaibhav_mehta/calculator-philomath.git <br>
Video: https://www.loom.com/share/cca533abfdf943868491de387bff4f2b

### Task-6 Background Generator
#### Resources
CODE: https://gitlab.com/_vaibhav_mehta/philomath-background-generator.git <br>
Video: https://www.loom.com/share/0572da64b1dc4952904f5c42e2921f89

### Task-7 rendering Dynamic counter 
### Resources
Video: https://www.loom.com/share/e1e41ece5c3145f6a70d360020f356f3 <br>
CODE: https://gitlab.com/_vaibhav_mehta/philomath-dynamic-counter.git 

### Task-8 Quiz APP
### Resources
#### DAY1
Video: https://www.loom.com/share/65e1692cb79f44ffa98372602305e5b5 <br>
CODE: https://gitlab.com/_vaibhav_mehta/philomath-quiz-app.git

#### DAY2
video: https://www.loom.com/share/66456ad67bdf4aa4997aeb162c7016e9  <br>
Code:  https://gitlab.com/_vaibhav_mehta/philomath-quiz-app.git

#### DAY3
Video: https://www.loom.com/share/6840042ab4a342a798d681db48da7cd9 <br>
Code: https://gitlab.com/_vaibhav_mehta/philomath-quiz-app.git <br>

#### DAY4
Video: https://www.loom.com/share/d390f2d00d1f4fe584617b30a38b529b <br>
Code: https://gitlab.com/_vaibhav_mehta/philomath-quiz-app.git  

#### DAY5
Video: https://www.loom.com/share/698db5c7bf1543d5b246bcbcb880ff42 <br>
Code: https://gitlab.com/_vaibhav_mehta/philomath-quiz-app.git  

#### AWS S3 Hosting for quiz app.
Video: https://www.loom.com/share/66a58a9fc6fe49ae9d92e5a10997faf1

### Task-9 3D product card
Video: https://drive.google.com/file/d/1VZwi1YOnHp6Ot5zL7BbtrZNqIDHYmHWr/view?usp=sharing<br>
Resource/Solution: https://www.youtube.com/watch?v=XK7T3mY1V-w&feature=youtu.be <br>

### Task-10 Currency converter using async await and promises
Video: https://youtu.be/mlb525FgU3k <br>
